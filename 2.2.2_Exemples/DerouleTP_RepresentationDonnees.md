- TP1 : introduction à la représentation binaire des données  (1h)

Lecture partielle. Présentation aux élèves.

Les élèves prennent individuellement connaissance du TP.
Le TP constitue une introduction au cours sur la représentation binaire des données

30 minutes sont accordées aux éléves pour répondre à chacune des questions posées.

A l'issue des 20 minutes et pour chauque question, un elève passe au tableau pour présenter son résultat + débat et participation des autres éleves (10 minutes)

Les éleves commencent à trouver les premières réponses de la question 1 en effectuant des soustractions successives.

Le remplissage de la première partie du tableau de la question 2 permet de visualiser les premières représentations binaires des nombres donnés en exemple, et de visualier ainsi les conversions de nombres décimaux en nombre binaires.
Sur la seconde partie du tableau, les éleves sont confrontés à une conversion de nombres binaires en nombres décimaux. Ils réalisent cette conversion en additionnant les nombres marqués à "1" dans le tableau.

La question 2c est l'une des questions cibles du TP, les élèves doivent comprendre que les valeurs données en tête du tableau correspondent aux premières puissances de 2

Le question 2e permet d'ouvrir le débat sur la méthode de conversion. Le professeur après avoir entendu les méthodes proposées par les élèves peut alors introduire au tableau la méthode euclidienne permettant de convertir un nombre décimal en nombre binaire.

A la fin du TP, le cours peut débuter avec:
    - une présentation du systeme décimal et la décomposition d'un nombre en puissance de 10
    - une présentation du système binaire avec des explication sur les conversions binaire>decimal et decimal>binaire
    => 15 minutes
    -quelques exercices de conversions pour ancrer les connaissances
    => 15 minutes




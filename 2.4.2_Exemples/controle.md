La suite d'exercices ci-dessous constitue une évaluation courte en classe d'une durée de 30 minutes

1. Convertir en binaires les nombres suivants données en base 10 (10 minutes):
458
133
47
1024
65

2. Convertir les nombres suivants (écrits en binaires) en décimal (10 minutes):
101010101
111000
00110011
1010010001

3. Ecrire un algorithme permettant de saisir un nombre décimal et de le convertir en binaire (10 minutes):

Bonus: implémenter l'alogorithme en pyhton
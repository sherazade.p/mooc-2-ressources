Thématique : Représentation des données
Notions liées : Représentation binaire d’un entier relatif
Résumé de l’activité : Activité débranchée: découverte de la représentation binaire
Objectifs : Découverte de la logique binaire et calcul binaire de nombres entiers non signés
Durée de l’activité : 1/2h
Forme de participation : individuelle 
Matériel nécessaire : Papier, stylo !
Préparation : Aucune
Autres références :
Fiche élève cours : https://fabricenativel.github.io/Premiere/pdf/C2/C2-cours.pdf
Fiche élève activité : https://fabricenativel.github.io/Premiere/pdf/C2/C2-act1.pdf
Une activité d'approfondissement peut être proposée aux éleves ayant terminé avant les autres le TP sur la représentation de données.
L'activité consiste à tester la fonction bin de Python permettant de convertir un nombre en écriture binaire

Lancer Python en ligne de commande
Tester la fonction bin de Python, en affichant par exemple bin(201) et bin(57). 
Rapprocher les résultats obtenus avec la conversion en binaire de (201)10 et (57)10
Émettre une hypothèse sur cette fonction.
Valider votre hypothèse en faisant afficher l'aide de la fonction bin.